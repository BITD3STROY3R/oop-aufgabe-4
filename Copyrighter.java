/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;
import java.util.Date;

public class Copyrighter extends Adder {

	private String copyright;
	private Date date = new Date();
	private Collection<String> authors = new LinkedList<String>();
	
	/* Vorbedingung : comment != null, copyright != null, authors != null
	 * Nachbedingung: Initialisiert ein neues Copyrighter-Objekt mit
	 * dem Kommentartext comment, der Copyrightinformation copyright, und
	 * den Authoren authors.
	 */
	public Copyrighter(String comment, String copyright,
			Collection<String> authors) {
		super(Abs.start, comment);
		this.copyright = copyright;
		this.authors.addAll(authors);
	}
	
	/* Vorbedingung : author != null
	 * Nachbedingung: Fügt Author author zur Liste der Authoren hinzu.
	 */
	public Copyrighter addAuthor(String author) {
		if (!authors.contains(author)) {
			authors.add(author);
		}
		
		return this;
	}
	
	/* Vorbedingung : author != null
	 * Nachbedingung: Entfernt den Author author aus der Liste der Authoren.
	 */
	public Copyrighter removeAuthor(String author) {
		if (authors.contains(author)) {
			authors.remove(author);
		}
		
		return this;
	}
	
	/* Vorbedingung : copyright != null
	 * Nachbedingung: Setzt den Text der Copyrightinformation gleich copyright.
	 */
	public Copyrighter setCopyright(String copyright) {
		this.copyright = copyright;
		
		return this;
	}
	
	/* Vorbedingung : Date != null
	 * Nachbedingung: Setzt das Datum das für das Kommentar verwendet wird
	 * gleich date.
	 */
	public Copyrighter setDate(Date date) {
		this.date = date;
		
		return this;
	}
	
	/* Vorbedingung : indent >= 0
	 * Nachbedingung: Liefert einen String der einem Java konformen Kommentar
	 * mit einer Einrückung von inden Tabulationen in /*-Syntax entspricht.
	 */
	@Override
	protected String formatComment(String indent) {
		
		String comment = indent + "/* " + date;
		
		comment += "\n" + indent + " *";
		comment += "\n" + indent + " * Authors :";
		
		for (String author : authors) {
			comment += "\n" + indent + " * \t" + author;
		}
		if (!authors.isEmpty()) {
			comment += "\n" + indent + " *";
		}
		
		String[] lines = getText().split("\n");
		for (String line : lines) {
			comment += "\n" + indent + " * " + line;
		}
		if (lines.length > 0) {
			comment += "\n" + indent + " *";
		}
		
		lines = copyright.split("\n");
		for (String line : lines) {
			comment += "\n" + indent + " * " + line;
		}
		if (lines.length > 0) {
			comment += "\n";
		}
		
		return comment + indent + " */";
	}
}
