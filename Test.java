/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Collection;
import java.util.LinkedList;


public class Test {

	public static void main(String[] args) {
		testAdder();
		testCopyrighter();
		testPurifiers();
		testErasers();
		testComposer();
	}
	
	private static void testAdder() {
		System.out.println(makeHeader("Testing Adder"));
		
		String prog = "import java.util.*;\n\n"
				+ "// commentar\n\n"
				+ "/* commentar */\n\n"
				+ "\t\t Test test;\n"
				+ "/* line 1.1\n"
				+ " * line 2.1 */\n"
				+ "/* line 1\n"
				+ " * line 2 */"
				+ "public class Test {\n"
				+ "\tint a = 1;\n"
				+ "// before double\n"
				+ "\tdouble b = 2;\n"
				+ "\tString str = \"/* dummy comment */\";\n"
				+ "\tString str = \"// dummy comment\";\n"
				+ "\tString dummy = \"\";\n"
				+ "}";
		
		Adder[] adders = {new Adder(Adder.Rel.before, "my integer", "int"), 
				new Adder(Adder.Rel.after, "dummy detected", "dummy"),
				new Adder(Adder.Rel.before, "test detected", "test"),
				new Adder(Adder.Abs.start, "absolute start comment"),
				new Adder(Adder.Abs.end, "absolute end comment")};
		
		for (Adder adder : adders) {
			System.out.println(adder.pretty(prog));
			System.out.println(makeSectionbar());
		}
	
		System.out.println("");
	}
	
	private static void testCopyrighter() {
		
		System.out.println(makeHeader("Testing Copyrighter"));
		
		String prog = "import java.util.*;\n\n"
				+ "// commentar\n\n"
				+ "/* commentar */\n\n"
				+ "\t\t Test test;\n"
				+ "/* line 1.1\n"
				+ " * line 2.1 */\n"
				+ "/* line 1\n"
				+ " * line 2 */"
				+ "public class Test {\n"
				+ "\tint a = 1;\n"
				+ "// before double\n"
				+ "\tdouble b = 2;\n"
				+ "\tString str = \"/* dummy comment */\";\n"
				+ "\tString str = \"// dummy comment\";\n"
				+ "\tString dummy = \"\";\n"
				+ "}";
		
		Collection<String> authors = new LinkedList<String>();
		authors.add("Alan Turing");
		
		Copyrighter[] adders = {
				new Copyrighter("Das ist ein Kommentar in einem Copyrighter",
				"Das ist die Copyright information", authors)};
		
		adders[0].addAuthor("Ada Lovelace");
		
		for (Adder adder : adders) {
			System.out.println(adder.pretty(prog));
			System.out.println(makeSectionbar());
		}
	
		System.out.println("");
	}
	
	private static void testPurifiers() {
		
		System.out.println(makeHeader("Testing Purifier"));
		
		String prog = "import java.util.*;\n\n"
				+ "// commentar\n\n"
				+ "/* commentar */\n\n"
				+ "\t\t Test test;\n"
				+ "/* line 1.1\n"
				+ " * line 2.1 */\n"
				+ "/* line 1\n"
				+ " * line 2 */"
				+ "public class Test {\n"
				+ "\tint a = 1;\n"
				+ "// before double\n"
				+ "\tdouble b = 2;\n"
				+ "\tString str = \"/* dummy comment */\";\n"
				+ "\tString str = \"// dummy comment\";\n"
				+ "\tString dummy = \"\";\n"
				+ "}";
		
		DeepPurifier[] purifiers = {
				new DeepPurifier(DeepPurifier.Style.complex, 3),
				new AltPurifier(DeepPurifier.Style.simple),
				new Purifier()
		};
		
		for (Prettifier prettifier : purifiers) {
			System.out.println(prettifier.pretty(prog));
			System.out.println(makeSectionbar());
		}
	
		System.out.println("");
	}
	
	private static void testErasers() {

		System.out.println(makeHeader("Testing Erasers"));
		
		String prog = "import java.util.*;\n\n"
				+ "// commentar\n\n"
				+ "/* commentar */\n\n"
				+ "\t\t Test test;\n"
				+ "/* line 1.1\n"
				+ " * line 2.1 */\n"
				+ "/* line 1\n"
				+ " * line 2 */"
				+ "public class Test {\n"
				+ "\tint a = 1;\n"
				+ "// before double\n"
				+ "\tdouble b = 2;\n"
				+ "\tString str = \"/* dummy comment */\";\n"
				+ "\tString str = \"// dummy comment\";\n"
				+ "\tString dummy = \"\";\n"
				+ "}";
		
		Eraser[] purifiers = {
				new Eraser(Adder.Rel.before, "class"),
				new Eraser(Adder.Rel.after, "class"),
				new Eraser(Adder.Abs.start),
				new Eraser(Adder.Abs.end),
				new Stripper()
		};
		
		for (Prettifier prettifier : purifiers) {
			System.out.println(prettifier.pretty(prog));
			System.out.println(makeSectionbar());
		}
	
		System.out.println("");
	}
	
	private static void testComposer() {

		System.out.println(makeHeader("Testing Composer"));
		
		String prog = "import java.util.*;\n\n"
				+ "// commentar\n\n"
				+ "/* commentar */\n\n"
				+ "\t\t Test test;\n"
				+ "/* line 1.1\n"
				+ " * line 2.1 */\n"
				+ "/* line 1\n"
				+ " * line 2 */"
				+ "public class Test {\n"
				+ "\tint a = 1;\n"
				+ "// before double\n"
				+ "\tdouble b = 2;\n"
				+ "\tString str = \"/* dummy comment */\";\n"
				+ "\tString str = \"// dummy comment\";\n"
				+ "\tString dummy = \"\";\n"
				+ "}";
		
		Collection<String> authors = new LinkedList<String>();
		authors.add("Alan Turing");
		authors.add("Ada Lovelace");
		
		Composer composer = new Composer();
		
		Prettifier[] prettifiers = {
				new Stripper(),
				new Adder(Adder.Abs.end, "end of programm"),
				new Copyrighter("Das ist ein Kommentar in einem Copyrighter",
						"Das ist die Copyright information", authors),
				new DeepPurifier(DeepPurifier.Style.simple, 1)
		};
		
		for (Prettifier prettifier : prettifiers) {
			composer.addPrettifier(prettifier);
		}
	
		System.out.println(composer.pretty(prog));
		System.out.println(makeSectionbar());
		System.out.println("");
	}
	
	private static final int pageWidth   = 80;
	private static final char headerChar = '=';
	private static final char sectionChar = '-';
	private static final int headerWidth = 80;
	private static final int labelMargin = 4;

	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert einen String bestehend aus der N-Fachen
	 * wiederholung des Zeichens c.
	 */
	private static String repeat(int n, char c) {
		String result = "";

		for (int i = 0; i < n; i++) {
			result += c;
		}

		return result;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String der maximal headerWidth Zeichen
	 * lang ist. Der String beginnt und endet mit maximal labelMargin vielen
	 * Leerzeichen. Die Anzahl der nachfolgenden Leerzeichen ist immer gleich
	 * der der Fuehrenden.
	 */
	private static String makeLabel(String label) {
		if (label.length() <= headerWidth) {
			int i = 0;
			while (i < labelMargin && label.length() <= headerWidth - 2) {
				label = " " + label + " ";
				i++;
			}
		} else {
			label = label.substring(0, headerWidth);
		}

		return label;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String welcher eine formatierte
	 * Ueberschrift darstellt. Label ist der Text der Ueberschrift.
	 */
	private static String makeHeader(String label) {
		String header = "";

		header += repeat(headerWidth, headerChar) + '\n';
		label = makeLabel(label);
		header += repeat((headerWidth - label.length()) / 2, headerChar);
		header += label;
		header += repeat((headerWidth - label.length()) / 2
				+ (headerWidth - label.length()) % 2, headerChar);
		header += '\n';
		header += repeat(headerWidth, headerChar);

		return header;
	}

	/* Nachbedingung: Liefert einen String welcher eine Abtrennungszeile
	 * darstellt. Diese Zeile hat eine Breite von genau pageWidth zeichen.
	 */
	private static String makeSectionbar() {
		return repeat(pageWidth, sectionChar);
	}

}
