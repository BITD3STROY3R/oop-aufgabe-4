/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.LinkedList;
import java.util.List;

public class Eraser implements Prettifier {
	
	private Adder.Abs abs;
	private Adder.Rel rel;
	
	private boolean relative = false;
	
	private String expression = "";
	
	/* Nachbedingung: Initialisiert ein neues Eraser-Objekt, welches durch ein
	 * Aufruf von pretty() am Anfang oder am Ende des Programms ein Kommentar
	 * entfernt.
	 */
	public Eraser(Adder.Abs abs) {
		this.abs = abs;
	}
	
	/* Vorbedingung : expression != null
	 * Initialisiert ein neues Eraser-Objekt, welches durch den Aufruf von
	 * pretty() in jeder Zeile entweder vor oder nach jedem Vorkommen von
	 * expression ein Kommentar entfernt.
	 */
	public Eraser(Adder.Rel rel, String expression) {
		this.rel 		= rel;
		this.relative   = true;
		this.expression = expression;
	}

	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Entfernt kommentare in prog. 
	 */
	@Override
	public String pretty(String prog) {
		
		String result;
		
		if (relative) {
			result = prettifyRelative(prog);
		} else {
			result = prettifyAbsolute(prog);
		}
		
		return result;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '\"', prog entspricht
	 * einem Teil eines syntaktisch korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten nicht escaped '\"'
	 * Zeichens.
	 */
	private int skipString(String prog) {
		
		boolean escaped = false;
		int i = 1;
		
		while (prog.charAt(i) != '"' || escaped) {
			if (prog.charAt(i) == '\\' && !escaped) {
				escaped = true;
			} else {
				escaped = false;
			}
			
			i++;
		}
		
		return i + 1;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '/',
	 * prog.charAt(1) == '/', prog entspricht einem Teil eines syntaktisch
	 * korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten Newline Zeichens oder
	 * prog.length() falls es kein auf das Kommentar folgendes Newline gibt.
	 */
	private int skipShortComment(String prog) {
		
		int i = 2;
		
		while (i < prog.length()  && prog.charAt(i) != '\n') {
			i++;
		}
		
		return i;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '/',
	 * prog.charAt(1) == '*', prog entspricht einem Teil eines syntaktisch
	 * korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten Zeichens nach dem Ende
	 * eines Kommentars in /*-Syntax.
	 */
	private int skipLongComment(String prog) {
		
		int i = 2;
		
		while (prog.charAt(i) != '*' || prog.charAt(i+1) != '/') {
			i++;
		}
		
		return i + 2;
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Liefert eine Liste der Zeilen des Programms prog. Zeilen
	 * werden bei Newlines (inklusive) gebrochen. Mehrzeilige Kommentare in 
	 * /*-Syntax, stehen zur Gänze in einer Zeile.
	 */
	protected final List<String> splitLines(String prog) {
		
		int i = 0;
		
		while (i < prog.length() && prog.charAt(i) != '\n') {
			if ('"' == prog.charAt(i)) {
				i += skipString(prog.substring(i));
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '/' == prog.charAt(i+1)) {
				i += skipShortComment(prog.substring(i));
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '*' == prog.charAt(i+1)) {
				i += skipLongComment(prog.substring(i));
			} else {
				i++;
			}
		}
		
		if (i == prog.length()) {
			
			LinkedList<String> result = new LinkedList<String>();
			result.add(prog);
			
			return result;
		}
		
		List<String> result = splitLines(prog.substring(i + 1));
		result.add(0, prog.substring(0, i + 1));
		
		return result;
	}
	
	/* Vorbedingung : lines != null
	 * Nachbedingung: Liefert einen String, der der Verkettung aller Strings
	 * in lines entspricht. Die Reihenfolge der Verkettung entspricht der
	 * Reihenfolge der Strings in lines.
	 */
	protected String joinLines(List<String> lines) {
		
		String result = "";
		
		for (String line : lines) {
			result += line;
		}
		
		return result;
	}
	
	/* Vorbedingung : lines != null, 0 <= pos < lines.size(), die Zeichenketten
	 * in lines entsprechen in ihrer Reihenfolge einem syntaktisch korrektem 
	 * Java-Programm, mehrzeilige Kommentare stehen zur gänze in einer Zeile.
	 * Nachbedingung: Entfernt falls vorhanden entweder vor oder nach der Zeile
	 * pos ein Kommentar.
	 */
	private void removeRelative(List<String> lines, int pos) {
		switch (rel) {
			case before:
			if (pos - 1 >= 0 && containsComment(lines.get(pos - 1))) {
				lines.set(pos - 1, removeComment(lines.get(pos - 1)));
			}
			break;
			
			case after:
			if (pos + 1 < lines.size()
					&& containsComment(lines.get(pos + 1))) {
				lines.set(pos + 1, removeComment(lines.get(pos + 1)));
			}
			break;
		}
	}
	
	/* Vorbedingung : line != null, line entspricht einer oder mehreren Zeilen
	 * eines syntaktisch korrektem Java-Programms.
	 * Nachbedingung: Liefert true falls die Zeile line ein Kommentar enthält,
	 * sonst false.
	 */
	protected final boolean containsComment(String line) {
		
		boolean found = false;
		int i = 0;
		
		while (i < line.length() && !found) {
			if ('"' == line.charAt(i)) {
				i += skipString(line.substring(i));
			} else if (i + 1 < line.length() && '/' == line.charAt(i)
					&& '/' == line.charAt(i + 1)) {
				found = true;
			} else if (i + 1 < line.length() && '/' == line.charAt(i)
					&& '*' == line.charAt(i + 1)) {
				found = true;
			} else {
				i = i + 1;
			}
		}
		
		return found;
	}
	
	/* Vorbedingung : line != null, line entspricht einer oder mehreren Zeilen
	 * eines syntaktisch korrektem Java-Programms.
	 * Nachbedingung: Liefert die Zeile line in der das erste auffindbare
	 * Kommentar entfernt wurde.
	 */
	protected final String removeComment(String line) {
		
		boolean found = false;
		int i = 0;
		
		while (i < line.length() && !found) {
			if ('"' == line.charAt(i)) {
				i += skipString(line.substring(i));
			} else if (i + 1 < line.length() && '/' == line.charAt(i)
					&& '/' == line.charAt(i + 1)) {
				found = true;
			} else if (i + 1 < line.length() && '/' == line.charAt(i)
					&& '*' == line.charAt(i + 1)) {
				found = true;
			} else {
				i = i + 1;
			}
		}
		
		if (found) {
			int j = skipComment(line.substring(i));
			line = line.substring(0, i) + line.substring(i + j);
			line = (line.trim().equals("")) ? "" : line;
		}
		
		return line;
	}
	
	/* Vorbedingung : comment != null, comment.charAt(0) == '/',
	 * comment.charAt(1) = '/' oder '*', comment entspricht einem Teil eines
	 * syntaktisch korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des ersten Zeichens nach dem Kommentar,
	 * oder line.length falls das Kommentar sich bis an das Ende von line
	 * erstreckt.
	 */
	private int skipComment(String comment) {
		
		int result = 0;
		
		if ("//".equals(comment.substring(0,2))) {
			result = skipShortComment(comment);
		} else {
			result = skipLongComment(comment);
		}
		
		return result;
	}
	
	/* Vorbedingung : prog != null, expr != null, prog entspricht einer oder 
	 * mehreren Zeile eines syntaktisch korrektem Java-Programm.
	 * Nachbedingung: Liefert true falls die Zeichenkette expr außerhalb
	 * eines String-Litterals oder eines Kommentars in prog vorkommt.
	 */
	private boolean matched(String prog, String expr) {
		
		int i = 0;
		int j = 0;
		
		while (i < prog.length() && j < expr.length()) {
			if ('"' == prog.charAt(i)) {
				i += skipString(prog.substring(i));
				j = 0;
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '/' == prog.charAt(i+1)) {
				i += skipShortComment(prog.substring(i));
				j = 0;
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '*' == prog.charAt(i+1)) {
				i += skipLongComment(prog.substring(i));
				j = 0;
			} else if (prog.charAt(i) == expr.charAt(j)) {
				j++;
				i++;
			} else {
				j = 0;
				i++;
			}
 		}
		return j == expr.length();
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Entfernt mehrere Kommentare aus prog.
	 */
	private String prettifyRelative(String prog) {
		
		List<String> lines = splitLines(prog);
		
		for (int i = 0; i < lines.size(); i++) {
			if (matched(lines.get(i), expression)) {
				removeRelative(lines, i);
			}
		}
		
		return joinLines(lines);
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Entfernt falls vorhanden entweder am Anfang oder am Ende 
	 * von prog ein Kommentar.
	 */
	private String prettifyAbsolute(String prog) {
		
		List<String> lines = splitLines(prog);
		
		switch (abs) {
			case start:
			removeStart(lines);
			break;
			
			case end:
			removeEnd(lines);
			break;
		}
		
		return joinLines(lines);
	}
	
	/* Vorbedingung : lines != null
	 * Nachbedingung: Entfernt das erste Kommentar in lines, vor welchem nur
	 * Whitespaces und leere Zeilen stehen.
	 */
	private void removeStart(List<String> lines) {
		
		int i = 0;
		
		while (i < lines.size() && lines.get(i).trim().equals("")) {
			i++;
		}
		
		String comment = lines.get(i).trim().substring(0, 2);
		
		if (comment.equals("//") || comment.equals("/*")) {
			lines.set(i, removeComment(lines.get(i)));
		}
	}
	
	/* Vorbedingung : lines != null
	 * Nachbedingung: Entfernt falls vorhanden das Kommentar, welches nur
	 * von Whitespaces oder leeren Zeilen gefolgt wird.
	 */
	private void removeEnd(List<String> lines) {
		
		int i = lines.size() - 1;
		
		while (i >= 0 && lines.get(i).trim().equals("")) {
			i--;
		}
		
		if (lines.get(i).length() > 2) {
			String comment = lines.get(i).trim().substring(0, 2);
			
			if (comment.equals("//") || comment.equals("/*")) {
				lines.set(i, removeComment(lines.get(i)));
			}
		}
	}
}
