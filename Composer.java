/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.ArrayList;

public class Composer implements Prettifier {

	private ArrayList<Prettifier> prettifiers = new ArrayList<Prettifier>();
	
	/* Vorbedingung: prog ist ein Java Programm
	 * Nachbedingung: führt konsekutiv alle prettifiers
	 * auf prog aus. Falls prettifiers leer ist, bleibt
	 * prog unverändert.
	 */
	@Override
	public String pretty(String prog) {
		
		for (Prettifier p : prettifiers) {
			prog = p.pretty(prog);
		}
		
		return prog;
	}
	
	/* Vorbedingung: p != null
	 * Nachbedingung: Fügt p am Ende der Liste hinzu.
	 */
	public Composer addPrettifier(Prettifier p) {
		prettifiers.add(p);
		return this;
	}
	
	/* Vorbedingung: p != null, 0 <= i < this.size()
	 * Nachbedingung: Fügt p am index i der Liste hinzu
	 */
	public Composer addPrettifier(Prettifier p, int i) {
		prettifiers.add(i, p);
		return this;
	}
	
	/* Vorbedingung: 0 <= i < this.size()
	 * Nachbedingung: Entfernt den Prettifier der sich am index i befindet.
	 */
	public Composer removePrettifier(int i) {
		prettifiers.remove(i);
		return this;
	}
	
	/* Nachbedingung: Liefert die aktuelle größe der liste der gespeicherten
	 * Prettifier Objekte.
	 */
	public int size() {
		return prettifiers.size();
	}
}
