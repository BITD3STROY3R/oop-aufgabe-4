/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.LinkedList;
import java.util.List;

public class Adder implements Prettifier {

	public static enum Abs { start, end }
	public static enum Rel { before, after }
	
	private Rel rel = Rel.before;
	private Abs abs = Abs.start;
	
	private boolean relative = false;
	
	private String comment;
	private String expression;
	
	/* Vorbedingung : comment != null
	 * Nachbedingung: Initialisiert ein neues Adder Objekt mit der absoluten
	 * Einfügestelle abs, und dem Kommentartext comment.
	 */
	public Adder(Abs abs, String comment) {
		this.abs     = abs;
		this.comment = comment;
	}
	
	/* Vorbedingung : comment != null, expression != null
	 * Nachbedingung: Initialisiert ein neues Adder Objekt mit der relativen
	 * Einfügestelle um die Zeichenkette expression, und dem Kommentartext
	 * comment.
	 */
	public Adder(Rel rel, String comment, String expression) {
		this.rel        = rel;
		this.relative   = true;
		this.comment    = comment;
		this.expression = expression;
	}
	
	/* Vorbedingung : line != null, prog != null
	 * Nachbedingung: Fügt comment getrennt durch eine Newline vor oder
	 * nach line ein.
	 */
	private String insertRelative(String line, String comment) {
		switch (rel) {
			case before:
			line = comment + "\n" + line;
			break;
			
			case after:
			line = line + comment + "\n";
			break;
		}
		
		return line;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '\"', prog entspricht
	 * einem Teil eines syntaktisch korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten nicht escaped '\"'
	 * Zeichens.
	 */
	private int skipString(String prog) {
		
		boolean escaped = false;
		int i = 1;
		
		while (prog.charAt(i) != '"' || escaped) {
			if (prog.charAt(i) == '\\' && !escaped) {
				escaped = true;
			} else {
				escaped = false;
			}
			
			i++;
		}
		
		return i + 1;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '/',
	 * prog.charAt(1) == '/', prog entspricht einem Teil eines syntaktisch
	 * korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten Zeichens nach einem
	 * einzeiligen Kommentar.
	 */
	private int skipShortComment(String prog) {
		
		int i = 2;
		
		while (i < prog.length() && prog.charAt(i) != '\n') {
			i++;
		}
		
		return i;
	}
	
	/* Vorbedingung : prog != null, prog.charAt(0) == '/',
	 * prog.charAt(1) == '*', prog entspricht einem Teil eines syntaktisch
	 * korrektem Java Programms.
	 * Nachbedingung: Liefert den Index des nächsten Zeichens nach dem Ende
	 * eines Kommentars in /*-Syntax.
	 */
	private int skipLongComment(String prog) {
		
		int i = 2;
		
		while (prog.charAt(i) != '*' || prog.charAt(i+1) != '/') {
			i++;
		}
		
		return i + 2;
	}
	
	/* Nachbedingung: Liefert ein Java konformes Kommentar */
	protected String formatComment(String indent) {
		
		String [] lines  = comment.split("\n");
		String formatted = indent + "/* " + lines[0];
		
		for (int i = 1; i < lines.length; i++) {
			formatted += "\n" + indent + " * " + lines[i];
		}
		
		formatted += (lines.length > 1) ? "\n" + indent : "";
		formatted += " */";
		
		return formatted;
	}
	
	/* Vorbedingung : prog != null, expr != null, prog entspricht einer oder 
	 * mehreren Zeile eines syntaktisch korrektem Java-Programm.
	 * Nachbedingung: Liefert true falls die Zeichenkette expr außerhalb
	 * eines String-Litterals oder eines Kommentars in prog vorkommt.
	 */
	private boolean matched(String prog, String expr) {
		
		int i = 0;
		int j = 0;
		
		while (i < prog.length() && j < expr.length()) {
			if ('"' == prog.charAt(i)) {
				i += skipString(prog.substring(i));
				j = 0;
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '/' == prog.charAt(i+1)) {
				i += skipShortComment(prog.substring(i));
				j = 0;
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '*' == prog.charAt(i+1)) {
				i += skipLongComment(prog.substring(i));
				j = 0;
			} else if (prog.charAt(i) == expr.charAt(j)) {
				j++;
				i++;
			} else {
				j = 0;
				i++;
			}
 		}
		return j == expr.length();
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch 
	 * korrektem Java-Programm.
	 * Nachbedingung: Fügt Kommentare in das Programm prog ein. Kommentare
	 * stehen in eigenen Zeilen.
	 */
	private String prettifyRelative(String prog) {
		
		List<String> lines = splitLines(prog);
		
		for (int i = 0; i < lines.size(); i++) {
			if (matched(lines.get(i), expression)) {
				
				String indent = createIndent(lines.get(i));
				
				lines.set(i, insertRelative(lines.get(i),
						formatComment(indent)));
			}
		}
		
		return joinLines(lines);
	}
	
	/* Vorbedingung : line != null
	 * Nachbedingung: Liefert einen String, der der Einrückung der Zeile
	 * line entspricht.
	 */
	private String createIndent(String line) {
		int i = 0;
		String indent = "";
		
		while (i < line.length()
				&& (line.charAt(i) == ' ' || line.charAt(i) == '\t')) {
			indent += line.charAt(i);
			i++;
		}
		
		return indent;
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Liefert eine Liste der Zeilen des Programms prog. Zeilen
	 * werden bei Newlines gebrochen. Mehrzeilige Kommentare in /*-Syntax
	 * stehen zur Gänze in einer Zeile.
	 */
	private List<String> splitLines(String prog) {
		
		int i = 0;
		
		while (i < prog.length() && prog.charAt(i) != '\n') {
			if ('"' == prog.charAt(i)) {
				i += skipString(prog.substring(i));
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '/' == prog.charAt(i+1)) {
				i += skipShortComment(prog.substring(i));
			} else if (i + 1 < prog.length() && '/' == prog.charAt(i)
					&& '*' == prog.charAt(i+1)) {
				i += skipLongComment(prog.substring(i));
			} else {
				i++;
			}
		}
		
		if (i == prog.length()) {
			
			LinkedList<String> result = new LinkedList<String>();
			result.add(prog);
			
			return result;
		}
		
		List<String> result = splitLines(prog.substring(i + 1));
		result.add(0, prog.substring(0, i + 1));
		
		return result;
	}
	
	/* Vorbedingung : lines != null
	 * Nachbedingung: Liefert einen String, der der Verkettung alles Strings
	 * in lines getrennt durch Newline entspricht.
	 */
	private String joinLines(List<String> lines) {
		
		String result = "";
		
		for (String line : lines) {
			result += line;
		}
		
		return result;
	}
	
	/* Vorbedingung : prog != null
	 * Nachbedingung: Fügt am Anfang oder am Ende von prog ein Kommentar
	 * ein.
	 */
	private String prettifyAbsolute(String prog) {
		
		String result = "";
		
		switch (abs) {
			case start:
			result = formatComment("") + "\n" + prog;
			break;
			
			case end:
			result = prog + "\n" + formatComment("");
			break;
		}
		
		return result;
	}
	
	/* Vorbedingung : prog != null, prog entspricht einem syntaktisch korrektem
	 * Java-Programm.
	 * Nachbedingung: Fügt zu dem Programm prog, eines oder mehrere Kommentar
	 * hinzu. Hinzugefügte Kommentare stehen in eigenen Zeilen.
	 */
	public String pretty(String prog) {
		if (relative) {
			return prettifyRelative(prog);
		}
		
		return prettifyAbsolute(prog);
	}
	
	/* Vorbedingung : comment != null
	 * Nachbedingung: Setzt den Text des einzufügenden Kommentars gleich 
	 * comment.
	 * SCHC         : Der Text comment wird ab dem nächsten aufruf von pretty()
	 * verwendet.
	 */
	public void setText(String comment) { this.comment = comment; }
	
	/* Nachbedingung: Liefert den Text den die durch pretty eingefügten
	 * Kommentare enthalten.
	 */
	protected String getText() { return comment; }
}
