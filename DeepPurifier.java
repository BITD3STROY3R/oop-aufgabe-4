/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 4 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.ArrayList;

public class DeepPurifier implements Prettifier {
	
	public static enum Style { simple, mixed, complex };
	
	private int indent;
	private Style style;
	
	/* Vorbedingung : indent >= 0
	 * Nachbedingung: Initialisiert ein neues DeepPurifier Objekt mit der
	 * Einrückung indent, und dem Formatierungsstyle style.
	 */
	public DeepPurifier(Style style, int indent) {
		this.indent = indent;
		this.style  = style;
	}
	
	/* Vorbedingung: text != null, indent >= 0
	 * Nachbedingung: Liefert ein java-konformes Kommentar das die
	 * Zeichen von text beihaltet. Das resultat hat keine führenden
	 * Whitespaces und ist nicht mit Newline beendet. Das Kommentar
	 * hat eine Einrückung von genau indent Tabulationen. Das Kommentar
	 * ist entweder einfach, komplex oder gemischt Formatiert.
	 */
	private String format(String text, int indent) {
		
		String comment = "";
		
		switch(style) {
			case simple:
				comment = formatSimple(text, indent);
			break;
			
			case mixed:
				comment = formatMixed(text, indent);
			break;
			
			case complex:
				comment = formatComplex(text, indent);
			break;
		}
		
		return comment;
	}
	
	/* Vorbedingung : text != null, indent >= 0
	 * Nachbedingung: Liefert den Text text formatiert als Java konformes
	 * Kommentar. Falls text nur eine Zeile umfasst wird das daraus
	 * resultierende Kommentar in //-Syntax formatiert, sonst in /*-Syntax.
	 * Die Einrückung jeder Zeile des Kommentars beträgt genau indent.
	 */
	private String formatMixed(String text, int indent) {
		
		String comment = "";
		
		if (-1 == text.indexOf('\n')) {
			comment = formatSimple(text, indent);
		} else {
			comment = formatComplex(text, indent);
		}
		
		return comment;
	}
	
	/* Vorbedingung: comment != null, indent >= 0
	 * Nachbedingung: Liefert den Text comment formatiert als Java konformes
	 * mehrzeiliges Kommentar in //-Syntax. Die Einrückung jeder Zeile des 
	 * Kommentars beträgt genau indent Tabulationen
	 */
	private String formatSimple(String text, int indent) {
		
		String lines[] 	 = text.split("\n");
		String comment 	 = ""; 
		String strIndent = repeat('\t', indent);
		
		
		for (int i = 0; i < lines.length; i++) {
			comment += strIndent + "// " + lines[i] + "\n";
		}
		
		return comment.substring(0, comment.length() - 1);
	}
	
	/* Vorbedingung: comment != null, indent >= 0
	 * Nachbedingung: Liefert den Text comment formatiert als Java konformes
	 * mehrzeiliges Kommentar in /*-Syntax. Die Einrückung jeder Zeile des 
	 * Kommentars beträgt genau indent Tabulationen
	 */
	private String formatComplex(String text, int indent) {
		
		String lines[] = text.split("\n");
		String strIndent = repeat('\t', indent);
		
		String result = strIndent + "/* " + lines[0];
		result += lines.length > 1 ? "\n" : "";
		
		for (int i = 1; i < lines.length; i++) {
			result += strIndent + " * " + lines[i] + "\n";
		}
		
		result += lines.length > 1 ? strIndent : "";
		result += " */";
		
		return result;
	}
	
	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert eine Zeichenkette bestehend aus n mal dem 
	 * Zeichen c.
	 */
	private String repeat(char c, int n) {
		
		String s = "";
		
		for (int i = 0; i < n; i++) {
			s += c;
		}
		
		return s;
	}
	
	/* Vorbedingung: prog ist ein java Programm
	 * Nachbedingung: Liefert das java Programm prog
	 * wobei alle vorhandenen Kommentare in eigene Zeilen gestellt wurden 
	 * und eine Einrückung größer gleich der der Vorherigen Zeile haben. 
	 * Kommentare sind entweder einfach, komplex oder gemischt Formatiert.
	 */
	public String pretty(String prog) {
		
		ArrayList<String> lines = outlineComments(prog);
		String result = "";
		int lineIndent = 0;
		
		int i = 0;
		while (i < lines.size()) {
			if (isSingleLineComment(lines.get(i))) {
				String text = "";
				/* groupiere //-Kommentare */
				while (i < lines.size() && isSingleLineComment(lines.get(i))) {
					text += extractSingleLine(lines.get(i)) + "\n";
					i++;
				}
				text = text.trim();
				result += format(text, indent + lineIndent) + "\n";
			} else if (isMultiLineComment(lines.get(i))) {
				result += format(extractMultiLine(lines.get(i)),
						indent + lineIndent) + "\n";
				i++;
			} else {
				if (!lines.get(i).trim().equals("")) {
					lineIndent = computeIndent(lines.get(i));
				}
				result += lines.get(i);
				i++;
			}
		}
		
		return result;
	}
	
	/* Vorbedingung: prog ist ein syntaktisch korrektes java Programm
	 * Nachbedingung: Liefert eine Liste von Strings, die in ihrer Gesamtheit
	 * ganz prog umfassen. Jedes Kommentar (inklusive syntaktische formatierung)
	 * steht in einem eigenen String.
	 * Die Reihenfolge der Strings entspricht der Reihenfolge von prog
	 */
	private ArrayList<String> parseComments(String prog) {
		
		int i = 0;
		boolean escaped = false;
		boolean string  = false;
		String f = "";
		ArrayList<String> fragments = new ArrayList<String>();
		
		while (i < prog.length() - 1) {
			// behandle Strings und escaped Anfürhrungszeichen
			if (prog.charAt(i) == '\\') {
				escaped = !escaped;
				f += prog.charAt(i);
				i++;
			} else if (prog.charAt(i) == '"') {
				if (!escaped) {
					string = !string;
				} else {
					escaped = false;
				}
				f += prog.charAt(i);
				i++;
			// lese kommentare aus
			} else if (prog.charAt(i) == '/' && !string) {
				if (prog.charAt(i + 1) == '/') {
					fragments.add(f);
					f = parseSingleLineComment(prog, i);
					i += f.length();
					fragments.add(f);
					f = "";
				} else if (prog.charAt(i + 1) == '*') {
					fragments.add(f);
					f = parseMultiLineComment(prog, i);
					i += f.length();
					fragments.add(f);
					f = "";
				}
			} else {
				escaped = false;
				f += prog.charAt(i);
				i++;
			}
			
		}
		// nimm das letzte Zeichen mit
		if (i < prog.length()) {
			f += prog.charAt(i);
		}
		if (!f.equals("")) {
			fragments.add(f);
		}
		
		return fragments;
	}
	
	/* Vorbedingung: line != null
	 * Nachbedingung: Liefert einrückung des String line,
	 * wobei die einrückung gleich der Anzahl der führenden
	 * Tabulationen ist. Leerzeichen werden nicht gezählt
	 */
	private int computeIndent(String line) {
		
		int tabs = 0;
		while (tabs < line.length() && line.charAt(tabs) == '\t') {
			tabs++;
		}
		
		return tabs;
	}
	
	/* Vorbedingung: prog ist ein syntaktisch korrektes java Programm
	 * 0 <= i < prog.length() - 2,
	 * syntaktisches einzeiliges Kommentar beginnt bei i
	 * Nachbedingung: Liefert alle Zeichen des Kommentars (inklusive
	 * formatierung) 
	 */
	private String parseSingleLineComment(String prog, int i) {
		String comment = "";
		
		// nimm die Kommentarsyntax "//"
		comment += prog.charAt(i);
		comment += prog.charAt(i + 1);
		i += 2;
		
		// nimm alle Zeichen bis zum Ende der Zeile
		while (i < prog.length() && prog.charAt(i) != '\n') {
			comment += prog.charAt(i);
			i++;
		}
		// nimm Zeilenendzeichen mit
		if (i < prog.length() && prog.charAt(i) == '\n') {
			comment += prog.charAt(i);
		}
		
		return comment;
	}
	
	/* Vorbedingung: prog ist ein syntaktisch korrektes java Programm
	 * 0 <= i < prog.length() - 4,
	 * syntaktisches mehrzeiliges Kommentar beginnt bei i
	 * Nachbedingung: Liefert alle Zeichen des Kommentars (inklusive
	 * formatierung) 
	 */
	private String parseMultiLineComment(String prog, int i) {
		String comment = "";
		
		// Kommentarsyntax prefix
		comment += prog.charAt(i);
		comment += prog.charAt(i+1);
		i += 2;
		
		// übernimm alle enthaltenen Zeichen
		while(prog.charAt(i) != '*' || prog.charAt(i + 1) != '/') {
			comment += prog.charAt(i);
			i++;
		}
		// Kommentarsyntax suffix
		comment += prog.charAt(i);
		comment += prog.charAt(i + 1);
		
		// Zeilenendzeichen
		if (prog.length() > i + 2 && prog.charAt(i + 2) == '\n') {
			comment += prog.charAt(i + 2);
		}
		
		return comment;
	}
	
	/* Vorbedingung: s != null
	 * Nachbedingung: Liefert true falls s einem syntaktischem einzeiligem 
	 * Java-Kommentar entspricht.
	 */
	private boolean isSingleLineComment(String s) {
		if (s.length() >= 2 && s.charAt(0) == '/' && s.charAt(1) == '/') {
			return true;
		}
		return false;
	}
	
	/* Vorbedingung: s != null
	 * Nachbedingung: Liefert true falls die formatierung von s
	 * einem syntaktischem mehrzeiligem java Kommentar entspricht.
	 */
	private boolean isMultiLineComment(String s) {
		if (s.length() >= 4 && s.charAt(0) == '/' && s.charAt(1) == '*') {
			return true;
		}
		return false;
	}
	
	/* Vorbedingung: isSingleLineComment(comment) == true
	 * Nachbedingung: Liefert den Text des Kommentares ohne
	 * Kommentarformatierungen. Das Resultat enthält keine newline
	 * Zeichen und beginnt nicht mit Whitespaces.
	 */
	private String extractSingleLine(String comment) {
		
		String s = "";
	
		s = comment.substring(2);
		s = s.substring(0, s.length() - 1);
		
		return s.trim();
	}
	
	/* Vorbedingung: isMultiLineComment(comment) == true
	 * Nachbedingung: Liefert den Text des Kommentarformatierungen,
	 * Der String ist möglicherweise mehrzeilig. 
	 * Zeilen haben keine führenden Whitespaces,
	 * die letzte Zeile ist nicht durch ein Newline beendet.
	 */
	private String extractMultiLine(String comment) {
		
		String lines[] = comment.split("\n");
		
		lines[0] = lines[0].substring(2);
		
		// entferne formatierungen
		for (int i = 0; i < lines.length; i++) {
			
			lines[i] = lines[i].trim();
			
			if (lines[i].length() > 0 
					&& lines[i].charAt(0) == '*' 
					&& !lines[i].equals("*/")) {
				lines[i] = lines[i].substring(1).trim();
			}
		}
		
		int i = lines.length - 1;
		
		// behandle verschieden mögliche Endungen
		if (lines[i].equals("*/")) {
			lines[i] = null;
		} else {
			lines[i] = lines[i].substring(0, lines[i].length() - 2).trim();
		}
		
		// fasse die Zeilen in ein Zeile zusammen
		String result = "";
		for (String s : lines) {
			if (s != null) {
				result += s + "\n";
			}
		}
		return result.trim();
	}
	
	/* Vorbedingung: line != null
	 * Nachbedingung: liefert true falls line entweder einem einzeiligem
	 * oder mehrzeiligem Kommentar entspricht.
	 */
	private boolean isComment(String line) {
		return isSingleLineComment(line) || isMultiLineComment(line);
	}
	
	/* Vorbedingung: jedes Kommentare stehen in einer eigenen Zeile,
	 * jede zeile die kein kommentar ist hat genau ein Newline an letzter stelle
	 * oder enthält kein Newline
	 * Nachbedingung: Zeilen die den einrückungen der Kommentare, bzw den
	 * whitespaces zwischen Kommentaren entsprechen sind entfernt.
	 */
	private ArrayList<String> stripCommentIndent(ArrayList<String> prog) {
		
		ArrayList<String> result = new ArrayList<String>(); 
		
		for (int i = 0; i < prog.size() - 1; i++) {
			String line = prog.get(i);
			if (!isComment(prog.get(i+1)) || line.indexOf('\n') != -1 
					|| !line.trim().equals("")) {
				result.add(line);
			}
		}
		result.add(prog.get(prog.size() - 1));
		
		return result;
	}
	
	/* Vorbedingung: prog != null, kommentare haben einen
	 * eigenen index
	 * Nachbedingung: Kommentare bleiben unverändert, alle Strings
	 * enthalten entweder genau ein newline am letzten index, oder
	 * haben kein Newline. Die Reihenfolge der Strings entspricht
	 * der von prog.
	 */
	private ArrayList<String> splitLines(ArrayList<String> prog) {
		ArrayList<String> result = new ArrayList<String>();
		
		for (String s : prog) {
			if (!isComment(s)) {
				int index = s.indexOf('\n');
				// zerlege den string an den Zeilenumbrüchen
				while(index != -1) {
					result.add(s.substring(0, index + 1));
					s = s.substring(index + 1);
					index = s.indexOf('\n');
				}
				result.add(s);
			} else {
				result.add(s);
			}
		}
		
		return result;
	}
	
	/* Vorbedingung: prog ist ein syntakisch korrektes java Programm
	 * Nachbedingung: Liefert eine Liste von strings die alle Zeilen von prog
	 * enthält und in der jedes Kommentar einen relativ zur vorkommen
	 * aufsteigenden index erhält. Jede zeile die kein Kommentar ist, ist
	 * durch ein Newline beendet
	 */
	private ArrayList<String> outlineComments(String prog) {
		
		ArrayList<String> lines 
			= stripCommentIndent(splitLines(parseComments(prog)));
		int i = 1;
		
		while (i < lines.size()) {
			if (i >= 1) {
				String line = lines.get(i);
				String prev = lines.get(i - 1);
				// schiebe Kommentare nach oben bis zu einem Kommentar oder \n
				if (isComment(line) && !isComment(prev) 
						&& prev.indexOf('\n') == -1) {
					lines.set(i - 1, line);
					// letzte Zeile des Programms
					if (isSingleLineComment(line)) {
						lines.set(i, prev + "\n");
					} else {
						lines.set(i, prev);
					}
					i--;
				} else {
					i++;
				}
			} else {
				i++;
			}
		}
		
		return joinLines(lines);
	}
	
	/* Vorbedingung: lines != null,
	 * lines enthält kommentare in eigenen Strings
	 * kommentare kommen nicht direkt nach nicht newline-beedeten Zeilen
	 * Nachbedingung: alle nicht kommentar Zeilen werden wurden die nicht
	 * mit Newline enden werden falls vorhanden mit der darauffolgenden Zeile
	 * verknüpft. Resultat enthält also keine unvollendeten Zeilen.
	 */
	private ArrayList<String> joinLines(ArrayList<String> lines) {
		
		ArrayList<String> result = new ArrayList<String>();
		int i = 0;
		
		while (i < lines.size()) {
			if (!isComment(lines.get(i))
					&& lines.get(i).indexOf('\n') == -1) {
				String line = "";
				// verkette aufeinanderfolgende Zeilenstücke
				while (i < lines.size() && !isComment(lines.get(i)) 
						&& lines.get(i).indexOf('\n') == -1) {
					line += lines.get(i);
					i++;
				}
				// übernimm letzte zeile, falls nicht schon geschehen
				if (i < lines.size()) {
					line += lines.get(i);
					i++;
				}
				result.add(line);
			}
			else {
				result.add(lines.get(i));
				i++;
			}
		}
		
		return result;
	}
}
